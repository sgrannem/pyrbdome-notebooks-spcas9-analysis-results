# pyRBDome-Notebooks - spCas9 analysis results

## Contents

- [Introduction](#introduction)

- [Repo Contents](#repo-contents)

- [License](./LICENSE)

- [Citation](#citation)

## Introduction

pyRBDome is a package for protein RNA-binding sites prediction. It combines multiple and distinct RNA-binding prediction algorithms to identify putative RNA-binding amino acids within proteins. The algorithms predict RNA-binding propensity from different aspects, for example using only the protein sequences or a combination fo protein sequence and protein structure. It then aggregates all the data into easily interpretable PDB and PDF data files, which can then be used to desing mutations for more detailed functional analyses. This repository contains all the results from the RBS-ID data analyses that we describe in the manuscript.

## Repo Contents
This repository contains the following content:

- [spCas9_RBS_ID_data.xlsx](./spCas9_RBS_ID_data.xlsx): spCas9 dataset used with the pyRBDome pipeline.
- [spCas9_sequence.fa](./spCas9_sequence.fa): spCas9 fasta file used for our analysing the RBD-ID data.

- [analysed_pdbs](./analysed_pdbs/): Directory containing all the PDB files and RNA-binding site predictions for each individual Uniprot IDs. If you want to run your own structure models, make sure that all the PDB files are stored in THIS directory and that their names are also included in the [protein_list.csv](./protein_list.csv) file! Below an explanation what all the different direactories are and what files they contain:

    - [pdb_files](./analysed_pdbs/Q99ZW2/pdb_files/)
     Location of the pdb file used for the analyses
    - [filtered_pdb_files](./analysed_pdbs/Q99ZW2/filtered_pdb_files/)
    This folder contains cleaned up versions of the PDB files.
    - [prediction_results](./analysed_pdbs/Q99ZW2/prediction_results/)
    This directory contains ALL the prediction results from the individual tools in b-factor columns of PDB files. The "FTMap_docked.pdb" files contain the coordinates for the molecules docked onto the structures by FTMap. The "FTMap_distances.pdb" files contain the distances (in Å) for each amino acid in the PDB file to the docked molecules in the b-factor column. In this directory also all the pymol sessions are stored that allows easy visualisation of the different PDB files in one Pymol session. The "analysis_results.pdf" PDF files contain all the prediction results in the protein sequence. The "model_predictions.pdb" files contain the probabilities for RNA-binding for each amino acid in the b-factor columns, calculated using our XGBoost model that was trained on the GT-Distance ground truth dataset (see the manuscript for more details). The "merged.pdb.zip" files contain the unprocessed PST-PRNA results.

- [analysis_results](./analysis_results/): Contains all the results from the statistical analyses that were performed, the distance analysis of the cross-linked peptides and amino acid sequences, the unprocessed prediction results from RNABindRPlus and DisoRDPbind, the Interproscan domain analysis results and fasta files for the individual protein sequences.

- [Notebooks](./Notebooks/): This directory contains:
    -  All the Jupyter notebooks that were used to analyse the spCas9 data. These can be used as a template for your own analyses.
