
import os
import sys
import pandas as pd
from biopandas.pdb import PandasPdb

pdb = PandasPdb().read_pdb(sys.argv[1])

#print(pdb.df['ATOM'].columns)

print(f"Minimum b-factor:\t{min(pdb.df['ATOM']['b_factor'].values)}\nMaximum b-factor:\t{max(pdb.df['ATOM']['b_factor'].values)}")
