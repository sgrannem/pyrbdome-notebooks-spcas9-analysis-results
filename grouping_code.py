import pymol
from pymol import cmd

# List to store matched object names
selected_objects = []

# Iterate over all object names
for obj_name in cmd.get_names("objects"):
    # Split the object name by underscores
    parts = obj_name.split('_')
    if len(parts) >= 3:
        # Check if the second part is 'A'
        if parts[1] == 'B':
            # Check if the third part is a single lowercase letter
            if len(parts[2]) == 1:
                selected_objects.append(obj_name)

# Group the selected objects
group_name = "XL_amino_acids"
cmd.group(group_name, " ".join(selected_objects))

# To make the group selection current (visible in the viewer), you can use:
cmd.show("cartoon", group_name)

# List to store matched object names
selected_objects = []

# Iterate over all object names
for obj_name in cmd.get_names("objects"):
    # Split the object name by underscores
    parts = obj_name.split('_')
    if len(parts) >= 3:
        # Check if the second part is 'A'
        if parts[1] == 'B   ':
            # Check if the third part is a single lowercase letter
            if len(parts[2]) > 1:
                selected_objects.append(obj_name)

# Group the selected objects
group_name = "XL_peptides"
cmd.group(group_name, " ".join(selected_objects))

# To make the group selection current (visible in the viewer), you can use:
cmd.show("cartoon", group_name)
